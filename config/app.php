<?php

return [
    // 应用调试模式
    'app_debug'      => true,
    // 应用Trace调试
    'app_trace'      => false,
    // URL参数方式 0 按名称成对解析 1 按顺序解析
    'url_param_type' => 1,

	// 任务状态
	'task_status' => [
		'0' => ['txt'=> '待审核', 'color'=> '#FFB800', 'status' => 'UNSTART', 'target_id' => '1'],
		'1' => ['txt'=> '开发中', 'color'=> '#FFB800', 'status' => 'PROCESSING', 'target_id' => ''],
		'2' => ['txt'=> '开发完成', 'color'=> '#5FB878', 'status' => 'PROCESSING', 'target_id'],
		'3' => ['txt'=> '已上线', 'color'=> '#5FB878', 'status' => 'PROCESSING', 'target_id',],
		'4' => ['txt'=> '已确认', 'color'=> '#009688', 'status' => 'FINISH', 'target_id',],
		'-1' => ['txt'=> '已关闭', 'color'=> '#c2c2c2', 'status' => 'FINISH', 'target_id'],
	],
//
];
