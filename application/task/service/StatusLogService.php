<?php

namespace app\task\service;

use think\facade\Session;
use think\Db;

/**
 * 需求、缺陷、任务状态修改记录
 */
class StatusLogService
{
	/**
	 *	新增修改记录
	 */
	public static function changeStatus($type, $id, $new_status)
	{
		if($type == 3){
			$db_name = Db::name('task');
		}elseif($type == 2){
			$db_name = Db::name('bug');
		}else{
			$db_name = Db::name('demand');
		}
		if($db_name->where('id', $id)->update(['status' => $new_status])){
			// 添加修改记录仪
			Db::name('status_log')->insert([
				'type' => $type,
				'user_id' => Session::get('user.id') ?? 0,
				'table_id' => $id,
				'status' => $new_status,
				'create_at' => date('Y-m-d H:i:s')
			]);
			return true;
		}else{
			return false;
		}
	}
}