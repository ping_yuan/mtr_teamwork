<?php

namespace app\task\service;

use think\Db;

/**
 * 任务需求相关服务
 */
class TaskService
{
    /**
     *  获取和任务相关的属性
     *  项目、优先级、任务状态、任务类型
     */
    public static function getAboutProperty()
    {
        $projects = Db::name('project')
            ->where(['is_deleted' => 0])
            ->cache('projects')
            ->select();
        $prioritys = Db::name('priority')
            ->where(['is_deleted' => 0])
            ->order('sort asc')
            ->cache('prioritys')
            ->select();
        $task_status = Db::name('task_status')
            ->where(['is_deleted' => 0])
            ->order('sort asc')
            ->cache('task_status')
            ->column('*','id');
        $task_types = [
            'DEMAND' => '开发需求',
            'BUG' => 'BUG缺陷'
        ];
        return compact('projects', 'prioritys', 'task_status', 'task_types');

    }
	/**
	 *	新增修改记录
	 */
	public static function changeStatus($type, $id, $new_status)
	{
		if($type == 3){
			$db_name = Db::name('task');
		}elseif($type == 2){
			$db_name = Db::name('bug');
		}else{
			$db_name = Db::name('demand');
		}
		if($db_name->where('id', $id)->update(['status' => $new_status])){
			// 添加修改记录仪
			Db::name('status_log')->insert([
				'type' => $type,
				'user_id' => Session::get('user.id') ?? 0,
				'table_id' => $id,
				'status' => $new_status,
				'create_at' => date('Y-m-d H:i:s')
			]);
			return true;
		}else{
			return false;
		}
	}
}