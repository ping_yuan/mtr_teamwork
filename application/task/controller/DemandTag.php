<?php

namespace app\wo\controller;

use controller\BasicAdmin;
use service\DataService;
use think\Db;

/**
 * 需求标签控制器
 */
class DemandTag extends BasicAdmin
{
	public $table = 'demand_tag';

	/**
	 *	列表
	 */
	public function index()
	{
		$this->title = '需求标签列表';
		$this->title = '项目域名';
		$get = $this->request->get();
		$db = Db::name($this->table)
			->alias('t1')
			->order('t1.id desc');
		if(isset($get['name']) && $get['name'] != ''){
			$db->whereLike('t1.name', "%{$get['name']}%");
		}
		return parent::_list($db);

	}

	/**
	 * 列表数据处理
	 */
	public function _index_data_filter(&$data)
	{

	}

	/**
	 * 新增
	 */
	public function add()
	{
		$this->title = '新增标签';
		return $this->_form($this->table, 'form');
	}

	/**
	 * 编辑
	 */
	public function edit()
	{
		$this->title = '修改标签';
		return $this->_form($this->table, 'form');
	}


	/**
	 * 删除
	 */
	public function del()
	{
		if (DataService::update($this->table)) {
			$this->success("删除成功！", '');
		}
		$this->error("删除失败！");
	}

	/**
	 *
	 */
	public function _form_filter(&$data)
	{
		if($this->request->isPost()){

		}else{

		}
	}
}