<?php

namespace app\task\controller;

use controller\BasicAdmin;
use service\DataService;
use think\Db;

/**
 *  优先级开发项目控制器
 */
class Priority extends BasicAdmin
{
	public $table = 'priority';

	/**
	 *	列表
	 */
	public function index()
	{
		$this->title = '优先级配置';
		$get = $this->request->get();
		$db = Db::name($this->table)->where(['is_deleted' => 0])->order('sort asc,id desc');
		return parent::_list($db);

	}

	/**
	 * 列表数据处理
	 */
	public function _index_data_filter(&$data)
	{

	}

	/**
	 * 新增
	 */
	public function add()
	{
		$this->title = '新增优先级';
		return $this->_form($this->table, 'form');
	}

	/**
	 * 编辑
	 */
	public function edit()
	{
		$this->title = '修改优先级';
		return $this->_form($this->table, 'form');
	}


	/**
	 * 删除
	 */
	public function del()
	{
		if (DataService::update($this->table)) {
			$this->success("删除成功！", '');
		}
		$this->error("删除失败！");
	}

	/**
	 * 表单处理
	 */
	public function _form_filter(&$data)
	{
		if($this->request->isPost()){

		    if($data['is_default'] == 1){
		        Db::name($this->table)->where('id', '>=', 1)->update(['is_default' => 0]);
            }
		}
	}
}