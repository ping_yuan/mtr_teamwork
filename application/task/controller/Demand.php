<?php

namespace app\task\controller;

use app\task\service\StatusLogService;
use think\facade\Config;
use think\facade\Session;
use controller\BasicAdmin;
use service\DataService;
use think\Db;


/**
 * 需求控制器
 */
class Demand extends BasicAdmin
{
	public $table = 'demand';

	public  $type =[
	    'DEMAND' => '开发需求',
        'BUG' => 'BUG缺陷'
    ];

	/**
	 *	列表
	 */
	public function index()
	{
		$this->title = '需求列表';
		$get = $this->request->get();
		$db = Db::name($this->table)
			->alias('t1')
			->leftJoin('project t2', 't1.project_id = t2.id')
			->leftJoin('system_user t3', 't1.create_user_id = t3.id')
            ->leftJoin('priority t4', 't1.priority_id = t4.id')
            ->where('t1.is_deleted', '0')
            ->field('t1.*,t2.name as project_name,t3.name as create_user,t4.name as priority_name,t4.color as priority_color')
            ->order('t1.id desc');
        if(isset($get['name']) && $get['name'] != ''){
            $db->whereLike('t1.title|t1.content', "%{$get['name']}%");
        }
        if(isset($get['project_id']) && $get['project_id'] != ''){
            $db->where('t1.project_id', $get['project_id']);
        }
        if(isset($get['priority_id']) && $get['priority_id'] != ''){
            $db->where('t1.priority_id', $get['priority_id']);
        }
        if(isset($get['type']) && in_array($get['type'], ['NEW','BUG'])){
            $db->where('t1.type', $get['type']);
        }
        if (isset($get['create_at']) && $get['create_at'] !== '') {
            list($start, $end) = explode(' - ', $get['create_at']);
            $db->whereBetween('t1.create_at', ["{$start} 00:00:00", "{$end} 23:59:59"]);
        }
        if (isset($get['status']) && $get['status'] !== '') {
            $db->where('t1.status', $get['status']);
        }
		return parent::_list($db);
	}

	/**
	 * 新增
	 */
	public function add()
	{
		$this->title = '新增需求';
		return $this->_form($this->table, 'form');
	}

	/**
	 * 编辑
	 */
	public function edit()
	{
		$this->title = '修改需求';
		return $this->_form($this->table, 'form');
	}

	/**
	 *	删除
	 */
	public function del()
	{
		if (DataService::update($this->table)) {
			$this->success("删除成功！", '');
		}
		$this->error("删除失败！");
	}

	/**
	 *	需求说明
	 */
	public function description()
	{
		$this->title = '需求说明';
		return $this->fetch();
	}

	/**
	 *	状态修改记录
	 */
	public function status_log()
	{
		$this->title = '处理记录';
		$id = $this->request->get('id');
		$status_log  = Db::name('status_log')
			->alias('t1')
			->leftJoin('system_user t2', 't1.user_id = t2.id')
			->where('table_id', $id)
			->field('t1.*,t2.name as create_user')
			->order('id desc')
			->select();
		if(isset($id) && !empty($status_log)){
			$demand_status = Config::get('app.demand_status');
			foreach ($status_log as $key=>$vo){
				$status_log[$key]['status_name'] = $demand_status[$vo['status']]['txt'];
			}
			$this->assign('status_log', $status_log);
			return $this->fetch();
		}else{
			$this->error('找不到相关数据');
		}
	}

	/**
	 *	审核(status 0->1)
	 */
	public function audit()
	{
		$id = $this->request->post('id');
		$demand  = Db::name($this->table)->where('id', $id)->find();
		if(!empty($demand) AND $demand['status'] == 0){
			if(StatusLogService::changeStatus(1, $id, 1)){
				$this->success('操作成功','');
			}else{
				$this->error('操作失败');
			}
		}else{
			$this->error('仅‘审核中’的需求才需要审核');
		}
	}

	/**
	 *	开发完成(status 1->2)
	 */
	public function finish()
	{
		$id = $this->request->post('id');
		$demand  = Db::name($this->table)->where('id', $id)->find();
		if(!empty($demand) AND $demand['status'] == 1){
			if(StatusLogService::changeStatus(1, $id, 2)){
				$this->success('操作成功','');
			}else{
				$this->error('操作失败');
			}
		}else{
			$this->error('仅‘开发中’的需求才需要审核');
		}
	}

	/**
	 *	上线(status 2->3)
	 */
	public function publish()
	{
		$id = $this->request->post('id');
		$demand  = Db::name($this->table)->where('id', $id)->find();
		if(!empty($demand) AND $demand['status'] == 2){
			if(StatusLogService::changeStatus(1, $id, 3)){
				$this->success('操作成功','');
			}else{
				$this->error('操作失败');
			}
		}else{
			$this->error('仅‘开发完成’的需求才需要审核');
		}
	}

	/**
	 *	上线确认(status 3->4)
	 */
	public function confirm()
	{
		$id = $this->request->post('id');
		$demand  = Db::name($this->table)->where('id', $id)->find();
		if(!empty($demand) AND $demand['status'] == 3){
			if(StatusLogService::changeStatus(1, $id, 4)){
				$this->success('操作成功','');
			}else{
				$this->error('操作失败');
			}
		}else{
			$this->error('仅‘已上线’的需求才需要审核');
		}
	}

	/**
	 * 关闭（status ->-1）
	 */
	public function close()
	{
		$id = $this->request->post('id');
		$demand  = Db::name($this->table)->where('id', $id)->find();
		if(!empty($demand) AND in_array($demand['status'], [1,2,3])){
			if(StatusLogService::changeStatus(1, $id, -1)){
				$this->success('操作成功','');
			}else{
				$this->error('操作失败');
			}
		}else{
			$this->error('当前状态无法关闭需求');
		}
	}

    /**
     * 列表数据处理
     */
    protected function _index_data_filter(&$data)
    {

		foreach ($data as &$vo) {
			$vo['create_at'] = date('Y-m-d',strtotime($vo['create_at']));
		}
        $projects = Db::name('project')
            ->where(['is_deleted' => 0])
            ->field('id,name')
            ->select();
        $prioritys = Db::name('priority')
            ->where(['is_deleted' => 0])
            ->field('id,name')
            ->select();
        $demand_status = Config::get('app.demand_status');
        $this->assign([
            'projects' => $projects,
            'prioritys'	=> $prioritys,
            'demand_status' => $demand_status

        ]);


    }

	/**
	 *  表单提交处理
	 */
    protected function _form_filter(&$data)
	{
		if($this->request->isPost()){
			if(isset($data['id']) AND $data['id'] != ''){
                // 将添加前记录保存到seesion
                $old_data = Db::name($this->table)->where(['id' => $data['id']])->find();
                !empty($old_data) && Session::set('demand_log', $old_data);
			}else{
				$data['create_user_id'] = Session::get('user.id');
				$data['status'] = 0;
			}
		}else{
			$projects = Db::name('project')
				->where(['is_deleted' => 0])
				->field('id,name')
				->select();
			$prioritys = Db::name('priority')
				->where(['is_deleted' => 0])
				->field('id,name,is_default')
				->select();
			$demand_status = Config::get('app.demand_status');
			$this->assign([
				'projects' => $projects,
				'prioritys' => $prioritys,
				'demand_status' => $demand_status
			]);
		}
		$this->assign('types',$this->type);
	}

	/**
     *  处理成功回跳处理
     */
    protected function _form_result($result, $data)
	{
		if ($result !== false) {
			// 添加修改记录
			$data_change_log = [
				'user_id' => Session::get('user.id'),
				'change_place' => '',
				'create_at' => date('Y-m-d H:i:s'),
			];
			if (isset($data['id']) AND $data['id'] != '') {
				// 更新返回
				$old_data = Session::pull('demand_log');
				if (!empty($old_data)) {
					$revisions = [
						'title' => $old_data['title'],
						'content' => $old_data['content'],
						'priority_id' => $old_data['priority_id'],
						'project_id' => $old_data['project_id'],
						'demand_file' => $old_data['demand_file']
					];
					$data_change_log['revisions'] = json_encode($revisions,JSON_UNESCAPED_UNICODE);
					$data_change_log['demand_id'] = $data['id'];
					$old_data['title'] != $data['title'] && $data_change_log['change_place'] .= '需求标题|';
					$old_data['priority_id'] != $data['priority_id'] && $data_change_log['change_place'] .= '优先级|';
					$old_data['demand_file'] != $data['demand_file'] && $data_change_log['change_place'] .= '需求文件|';
					$old_data['content'] != $data['content'] && $data_change_log['change_place'] .= '需求描述|';
					Db::name('demand_change_log')->insert($data_change_log);
				}
			}
		}
	}

}