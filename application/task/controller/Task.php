<?php

namespace app\task\controller;

use app\task\service\TaskService;
use app\task\service\TaskServiceService;
use think\facade\Session;
use controller\BasicAdmin;
use service\DataService;
use think\Db;

/**
 * 任务控制器
 */
class Task extends BasicAdmin
{
	public $table = 'task';

	/**
	 *	列表
	 */
	public function index()
	{

		$this->title = '任务清单';
		$get = $this->request->get();
		$db = Db::name($this->table)
			->alias('t1')
			->leftJoin('project t2', 't1.project_id = t2.id')
			->leftJoin('system_user t3', 't1.create_user_id = t3.id')
			->leftJoin('priority t4', 't1.priority_id = t4.id')
			->where('t1.is_deleted', '0')
			->field('t1.*,t2.name as project_name,t3.name as create_user,t4.name as priority_name,t4.color as priority_color')
			->order('t1.id desc');
		if(isset($get['name']) && $get['name'] != ''){
			$db->whereLike('t1.title|t1.content', "%{$get['name']}%");
		}
		if(isset($get['project_id']) && $get['project_id'] != ''){
			$db->where('t1.project_id', $get['project_id']);
		}
		if(isset($get['priority_id']) && $get['priority_id'] != ''){
			$db->where('t1.priority_id', $get['priority_id']);
		}
		if(isset($get['type']) && in_array($get['type'], ['NEW','BUG'])){
			$db->where('t1.type', $get['type']);
		}
		if (isset($get['create_at']) && $get['create_at'] !== '') {
			list($start, $end) = explode(' - ', $get['create_at']);
			$db->whereBetween('t1.create_at', ["{$start} 00:00:00", "{$end} 23:59:59"]);
		}
		if (isset($get['status']) && $get['status'] !== '' && in_array($get['status'], ['FINISH','PROCESSING','UNSTART'])) {
			$task_status = Db::name('task_status')->where('type', $get['status'])->column('id');
			$db->whereIn('task_status_id', $task_status);
		}
		return parent::_list($db);
	}

	/**
	 * 新增
	 */
	public function add()
	{
		$this->title = '新增任务';
		return $this->_form($this->table, 'form');
	}

	/**
	 * 编辑
	 */
	public function edit()
	{
		$this->title = '修改任务';
		return $this->_form($this->table, 'form');
	}


	/**
	 *	任务明细
	 */
	public function detail()
	{
		$this->title = '任务明细';
		$id = $this->request->get('id');
		// 基本信息
		$task = Db::name($this->table)
			->alias('t1')
			->leftJoin('project t2', 't1.project_id = t2.id')
			->leftJoin('system_user t3', 't1.create_user_id = t3.id')
			->leftJoin('priority t4', 't1.priority_id = t4.id')
			->field('t1.*,t2.name as project_name,t3.name as create_user,t4.name as priority_name,t4.color as priority_color')
			->where(['t1.id' => $id, 't1.is_deleted' => 0])
			->find();

        $this->assign(TaskService::getAboutProperty());
		$this->assign('title','任务明细');
		return $this->fetch();
	}

	/**
	 * 任务操作说明
	 */
	public function description()
	{
		$this->title = '操作说明';
		return $this->fetch();
	}

	/**
	 * 删除
	 */
	public function del()
	{
		if (DataService::update($this->table)) {
			$this->success("删除成功！", '');
		}
		$this->error("删除失败！");
	}

	/**
	 *	统计
	 */
	public function statistics()
	{
        $task_status = Db::name('task_status')
            ->where(['is_deleted' => 0])
            ->order('sort asc')
            ->column('*','id');
        dump($task_status);
        die();

	}



	/**
	 * 列表数据处理
	 */
	public function _index_data_filter(&$data)
	{
		foreach ($data as &$vo) {
			$vo['create_at'] = date('Y-m-d',strtotime($vo['create_at']));
		}
		$this->assign(TaskService::getAboutProperty());
	}

	/**
	 * 表单提交处理
	 */
	protected function _form_filter(&$data)
	{
		if($this->request->isPost()){
			if(isset($data['id']) AND $data['id'] != ''){
				// 将添加前记录保存到seesion
				$old_data = Db::name($this->table)->where(['id' => $data['id']])->find();
				!empty($old_data) && Session::set('task_log', $old_data);
			}else{
			    // 获取状态
                $data['task_status_id'] = Db::name('task_status')
                    ->where('is_default', 1)
                    ->column('id');
				$data['create_user_id'] = Session::get('user.id');
				$data['status'] = 0;
			}
		}else{
            $this->assign(TaskService::getAboutProperty());
		}
	}

	/**
	 *	表单处理后处理
	 */
	protected function _form_result($result, $data)
	{
		if ($result !== false) {
			// 添加修改记录
			$data_change_log = [
				'user_id' => Session::get('user.id'),
				'change_place' => '',
				'create_at' => date('Y-m-d H:i:s'),
			];
			if (isset($data['id']) AND $data['id'] != '') {
				// 更新返回
				$old_data = Session::pull('task_log');
				if (!empty($old_data)) {
					$revisions = [
						'title' => $old_data['title'],
						'content' => $old_data['content'],
						'priority_id' => $old_data['priority_id'],
						'project_id' => $old_data['project_id'],
					];
					$data_change_log['revisions'] = json_encode($revisions,JSON_UNESCAPED_UNICODE);
					$data_change_log['task_id'] = $data['id'];
					$old_data['title'] != $data['title'] && $data_change_log['change_place'] .= '需求标题,';
					$old_data['priority_id'] != $data['priority_id'] && $data_change_log['change_place'] .= '优先级,';
					$old_data['content'] != $data['content'] && $data_change_log['change_place'] .= '需求描述,';
                    rtrim($data_change_log['change_place'], ',');
					Db::name('task_change_log')->insert($data_change_log);
				}
			}
		}
	}
}