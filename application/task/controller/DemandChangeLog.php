<?php

namespace app\task\controller;

use controller\BasicAdmin;
use service\DataService;
use think\Db;

/**
 * 需求修改记录控制器
 */
class DemandChangeLog extends BasicAdmin
{
	public $table = 'demand_change_log';

	/**
	 *	列表
	 */
	public function index()
	{
		$this->title = '需求修改记录';

	}

	/**
	 * 列表数据处理
	 */
	public function _index_data_filter(&$data)
	{


	}

	/**
	 * 查看
	 */
	public function detail()
	{
		$this->title = '需求修改记录详情';
		return $this->_form($this->table, 'form');
	}




	/**
	 * 删除
	 */
	public function del()
	{
		if (DataService::update($this->table)) {
			$this->success("删除成功！", '');
		}
		$this->error("删除失败！");
	}

	/**
	 * 
	 */
	public function _form_filter(&$data)
	{
		if($this->request->isPost()){

		}else{

		}
	}
}