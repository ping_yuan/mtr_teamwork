<?php

namespace app\organize\controller;

use controller\BasicAdmin;
use service\DataService;
use think\Db;

/**
 * 开发项目控制器
 */
class Project extends BasicAdmin
{
	public $table = 'project';

	/**
	 *	列表
	 */
	public function index()
	{
		$this->title = '开发项目';
		$get = $this->request->get();
		$db = Db::name($this->table)
			->alias('t1')
			->leftJoin('project_team t2', 't1.team_id = t2.id')
            ->where('t1.is_deleted', '0')
			->field('t1.*,t2.name as team_name')
			->order('t1.id desc');
		if(isset($get['name']) && $get['name'] != ''){
			$db->whereLike('t1.name', "%{$get['name']}%");
		}
		return parent::_list($db);

	}

	/**
	 * 列表数据处理
	 */
	public function _index_data_filter(&$data)
	{
		$db_project_domain = Db::name('project_domain');
		foreach ($data as &$vo) {
			$domains_names = $db_project_domain->where(['project_id' => $vo['id']])->column('domain_name');
			$vo['domain_names'] = empty($domains_names) ? '' : join('|', $domains_names);
		}

	}

	/**
	 * 新增
	 */
	public function add()
	{
		$this->title = '新增项目';
		return $this->_form($this->table, 'form');
	}

	/**
	 * 编辑
	 */
	public function edit()
	{
		$this->title = '修改项目';
		return $this->_form($this->table, 'form');
	}


	/**
	 * 删除
	 */
	public function del()
	{
		if (DataService::update($this->table)) {
			$this->success("删除成功！", '');
		}
		$this->error("删除失败！");
	}

	/**
	 * s
	 */
	public function _form_filter(&$data)
	{
		if($this->request->isPost()){

		}else{
            $project_teams = Db::name('project_team')
                ->field('id,name')
                ->select();
            $this->assign(['project_teams' => $project_teams]);
		}
	}
}