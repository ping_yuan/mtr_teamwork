<?php

namespace app\organize\controller;

use controller\BasicAdmin;
use service\DataService;
use think\Db;

/**
 * 项目域名控制器
 */
class ProjectDomain extends BasicAdmin
{
	public $table = 'project_domain';

	/**
	 *	列表
	 */
	public function index()
	{

		$this->title = '项目域名';
		$get = $this->request->get();
		$db = Db::name($this->table)
			->alias('t1')
			->leftJoin('project t2', 't1.project_id = t2.id')
            ->where('t1.is_deleted', '0')
			->field('t1.*,t2.name as project_name')
			->order('t1.id desc');
		if(isset($get['domain_name']) && $get['domain_name'] != ''){
			$db->whereLike('t1.domain_name', "%{$get['domain_name']}%");
		}
		if(isset($get['project_id']) AND $get['project_id'] != ''){
		    $db->where('project_id', $get['project_id']);
        }
		return parent::_list($db);

	}

	/**
	 * 列表数据处理
	 */
	public function _index_data_filter(&$data)
	{
	    $projects = Db::name('project')
            ->where(['is_deleted' => 0])
            ->field('id,name')
            ->select();
	    $this->assign(['projects' => $projects]);
	}

	/**
	 * 新增
	 */
	public function add()
	{
		$this->title = '新增项目域名';
		return $this->_form($this->table, 'form');
	}

	/**
	 * 编辑
	 */
	public function edit()
	{
		$this->title = '修改项目域名';
		return $this->_form($this->table, 'form');
	}


	/**
	 * 删除
	 */
	public function del()
	{
		if (DataService::update($this->table)) {
			$this->success("删除成功！", '');
		}
		$this->error("删除失败！");
	}

	/**
	 * 表单数据过滤
	 */
	public function _form_filter(&$data)
	{
		if($this->request->isPost()){

		}else{

			$this->_form_assign();
		}
	}

	public function _form_assign()
	{
		$projects = Db::name('project')
			->where(['is_deleted' => 0])
			->field('id,name')
			->select();
		$this->assign(['projects' => $projects]);
	}
}