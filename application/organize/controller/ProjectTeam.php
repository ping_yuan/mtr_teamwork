<?php

namespace app\organize\controller;

use controller\BasicAdmin;
use service\DataService;
use think\Db;

/**
 * 开发团队控制器
 */
class ProjectTeam extends BasicAdmin
{
	public $table = 'project_team';

	/**
	 *	列表
	 */
	public function index()
	{
		$this->title = '开发团队';
		$get = $this->request->get();
		$db = Db::name($this->table)
            ->alias('t1')
            ->order('t1.id desc')
            ->leftJoin('system_user t2', 't1.leader_user_id = t2.id')
            ->field('t1.*,t2.name as leader_user_name');
		if(isset($get['name']) && $get['name'] != ''){
			$db->whereLike('t1.name', "%{$get['name']}%");
		}
		return parent::_list($db);
	}

	/**
	 * 列表数据处理
	 */
	public function _index_data_filter(&$data)
	{
		foreach ($data as &$vo) {
			$vo['user_count'] = count(explode(',', $vo['user_ids']));
			!$vo['user_ids'] && $vo['user_count'] = 0;
		}
	}

	/**
	 * 新增
	 */
	public function add()
	{
		$this->title = '新增团队';
		return $this->_form($this->table, 'form');
	}

	/**
	 * 编辑
	 */
	public function edit()
	{
		$this->title = '修改团队';
		return $this->_form($this->table, 'form');
	}


	/**
	 * 删除
	 */
	public function del()
	{
		if (DataService::update($this->table)) {
			$this->success("删除成功！", '');
		}
		$this->error("删除失败！");
	}

	/**
	 * 表单处理
	 */
	public function _form_filter(&$data)
	{
		if($this->request->isPost()){
			if(isset($data['user_ids']) && is_array($data['user_ids'])){
				$data['user_ids'] = join(',', $data['user_ids']);
			}else{
				$data['user_ids'] = '';
			}
			if(Db::name($this->table)->where(['name' => $data['name']])->count() > 0 AND !isset($data['id'])){
				$this->error('已经存在相同的名称，请更换');
			}

		}else{
			$data['user_ids'] = explode(',', isset($data['user_ids']) ? $data['user_ids'] : '');
			$programmer_auth_id = sysconf('programmer_auth_id');
			$users = Db::name('system_user')
				->where(['status' => 1, 'is_deleted' => 0])
				->whereLike('authorize', "%{$programmer_auth_id}%")
				->field('id,username,name,phone')->select();
			$this->assign('users', $users);

		}
	}


}